#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


cache = [0] * 500000


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert i > 0
    assert j > 0
    greatNum = max(i, j)
    lessNum = min(i, j)
    maxCycleLength = 0
    for x in range(lessNum, greatNum + 1):
        currentCycleLength = 0
        if x < 500000:
            if cache[x] == 0:
                cache[x] = collatz_calc(x)
            if (maxCycleLength < cache[x]):
                maxCycleLength = cache[x]
        else:
            currentCycleLength = collatz_calc(x)
            if(maxCycleLength < currentCycleLength):
                maxCycleLength = currentCycleLength
    assert maxCycleLength > 0
    return maxCycleLength

# ------------
# collatz_calc (student-made helper method)
# ------------


def collatz_calc(n):
    """ 
    n is the number that we are trying to find the cycle length of
    it will return the cycle length of the number, n. It used a lazy
    cache to be optimized to be ran efficiently. 
    """
    assert n > 0
    cycleLength = 1
    while n != 1:
        if n % 2 == 0:
            n = n // 2
            if n < 500000 and cache[n] != 0:
                # if cache[n] != 0 :
                cycleLength += cache[n]
                assert cycleLength > 1
                return cycleLength
        else:
            n = 3 * n + 1
            if n < 500000 and cache[n] != 0:
                cycleLength += cache[n]
                assert cycleLength > 1
                return cycleLength
        cycleLength += 1
    assert cycleLength > 0
    return cycleLength

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
